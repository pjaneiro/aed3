#include <cstdlib>
#include <cstdio>
#include <cfloat>
#include <cmath>

typedef struct
{
	float latitude, longitude;
} registry;

void insertionSort(registry*, int);
float truncate(float);

int main(int argc, char *argv[])
{
	int nLinhas, count, i;
	float auxLat, auxLon;
	fscanf(stdin,"%d",&nLinhas);
	registry* data = (registry*)malloc(nLinhas*sizeof(registry));			//reservar memória para os dados
	for(i=0;i<nLinhas;i++)
	{
		fscanf(stdin,"%*d,%*d,%*d-%*d-%*d %*d:%*d:%*d,%f,%f",&((data+i)->latitude),&((data+i)->longitude));
		(data+i)->latitude = truncate((data+i)->latitude);					//utiliza a função truncate para armazenar os dados
		(data+i)->longitude = truncate((data+i)->longitude);				//com duas casas decimais
	}
	insertionSort(data,nLinhas);											//ordena os dados
	for(i=0;i<nLinhas;i++)
	{
		if(i==0)															//primeiro valor, inicializa as variáveis usadas
		{
			auxLat = (data+i)->latitude;
			auxLon = (data+i)->longitude;
			count = 1;
		}
		else if((data+i)->latitude==auxLat && (data+i)->longitude==auxLon)	//mesma coordenada que a anterior, aumenta counter
		{
			count++;
		}
		else																//mudam as coordenadas, imprime e atualiza variáveis
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat,auxLon,count);
			auxLat = (data+i)->latitude;
			auxLon = (data+i)->longitude;
			count = 1;
		}
		if(i==nLinhas-1)													//último valor, imprime
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat,auxLon,count);
		}
	}
	free(data);																//liberta memória utilizada
	return 0;
}

float truncate(float val)													//função usada para armazenar variáveis com 2 casas decimais
{ 
	val *= 100;
    if (val>0)
    	return (float)floor(val)/100;
    else
    	return (float)ceil(val)/100;
}

void insertionSort(registry* data, int tamanho)
{
	registry temp;
	int i, j;
	for(i=1;i<tamanho;i++)													//percorre todos os elementos para comparação
	{
		temp = *(data+i);													//armazena o valor numa variável temporária
		for (j=i;j>0 && ((temp.latitude<(data+j-1)->latitude) || (temp.latitude==(data+j-1)->latitude && temp.longitude<(data+j-1)->longitude));j--)
		{																	//para todos os elementos, se forem maiores que temp,
			*(data+j)=*(data+j-1);											//são "empurrados" para a direita
		}
		*(data+j) = temp;													//coloca temp no local correto (até à iteração seguinte)
	}
}
