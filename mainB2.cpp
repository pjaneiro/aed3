#include <cstdlib>
#include <cstdio>
#include <cfloat>
#include <cmath>

typedef struct
{
	float latitude, longitude;												//os restantos elementos do input não são necessários
} registry;

float truncate(float);
void heapSort(registry*, int);

int main(int argc, char *argv[])
{
	int nLinhas, count, i;
	float auxLat, auxLon;
	fscanf(stdin,"%d",&nLinhas);
	registry* data = (registry*)malloc(nLinhas*sizeof(registry));			//reservar memória para os dados
	for(i=0;i<nLinhas;i++)
	{
		fscanf(stdin,"%*d,%*d,%*d-%*d-%*d %*d:%*d:%*d,%f,%f",&((data+i)->latitude),&((data+i)->longitude));
		(data+i)->latitude = truncate((data+i)->latitude);					//utiliza a função truncate para armazenar os dados
		(data+i)->longitude = truncate((data+i)->longitude);				//com duas casas decimais
	}
	heapSort(data,nLinhas);													//ordena os dados
	for(i=0;i<nLinhas;i++)
	{
		if(i==0)															//primeiro valor, inicializa as variáveis usadas
		{
			auxLat = (data+i)->latitude;
			auxLon = (data+i)->longitude;
			count = 1;
		}
		else if((data+i)->latitude==auxLat && (data+i)->longitude==auxLon)	//mesma coordenada que a anterior, aumenta counter
		{
			count++;
		}
		else																//mudam as coordenadas, imprime e atualiza variáveis
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat,auxLon,count);
			auxLat = (data+i)->latitude;
			auxLon = (data+i)->longitude;
			count = 1;
		}
		if(i==nLinhas-1)													//último valor, imprime
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat,auxLon,count);
		}
	}
	free(data);																//liberta memória utilizada
	return 0;
}

float truncate(float val)													//função usada para armazenar variáveis com 2 casas decimais
{ 
	val *= 100;
    if (val>0)
    	return (float)floor(val)/100;
    else
    	return (float)ceil(val)/100;
}

void heapSort(registry* data, int tamanho)
{
	int i=tamanho/2, parent, child;
	registry temp;

	while(1)																//Corre até todos os dados estarem ordenados
	{
		if (i>0)															//Primeiro passo: ordenar a "heap"
		{
			i--;
			temp = *(data+i);												//armazena o valor do pai numa variável temporária
		}
		else																//Segundo passo: Extrai elementos da "heap"
		{
			tamanho--;														//a "heap" fica mais pequena
			if (tamanho == 0)												//a "heap" já não tem mais elementos
				return;
			temp = *(data+tamanho);											//armazena o último valor
			*(data+tamanho) = *(data+0);									//guarda o valor mais elevado no fim dos dados
		}

		parent = i;															//começamos a verificação a partir do parent
		child = i*2 + 1;													//filho esquerdo

		while (child<tamanho)												//Empurrar o valor de temp pela "heap" abaixo
		{
			if((child+1)<tamanho && (((data+child+1)->latitude>(data+child)->latitude) || (((data+child+1)->latitude==(data+child)->latitude) && ((data+child+1)->longitude>(data+child)->longitude))))
			{																//Procura o filho maior
				child++;
			}
			if( ((data+child)->latitude>temp.latitude) || ( ((data+child)->latitude==temp.latitude) && ((data+child)->longitude>temp.longitude) ) )
			{																//Se algum filho for maior que o parent
				*(data+parent) = *(data+child);								//Move o filho para cima
				parent = child;												//Move o ponteiro do pai para o filho
				child = parent*2+1;											//Procura o próximo filho /* the previous line is wrong*/
			}
			else
			{
				break;														//temp está no sítio correto
			}
		}
		*(data+parent) = temp;												//colocamos temp na heap
	}
}
