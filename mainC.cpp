#include <cstdlib>
#include <cstdio>
#include <cfloat>
#include <cmath>

int truncate(float);
void radixSort(int*,int);

int main(int argc, char* argv[])
{
	int nLinhas, count, i;
	float auxLat, auxLon;
	fscanf(stdin,"%d",&nLinhas);
	int* data = (int*)malloc(nLinhas*sizeof(int));
	for(i=0;i<nLinhas;i++)
	{
		fscanf(stdin,"%*d,%*d,%*d-%*d-%*d %*d:%*d:%*d,%f,%f",&auxLat,&auxLon);
		data[i] = ((truncate(auxLat)*1000) + ((truncate(auxLon)+930)));
	}
	radixSort(data,nLinhas);
	for(i=0;i<nLinhas;i++)
	{
		if(i==0)															//primeiro valor, inicializa as variáveis usadas
		{
			auxLat = (float)data[i]/1000;
			auxLon = (float)((data[i]%1000)-930);
			count = 1;
		}
		else if((float)data[i]/1000==auxLat && (float)((data[i]%1000)-930)==auxLon)	//mesma coordenada que a anterior, aumenta counter
		{
			count++;
		}
		else																//mudam as coordenadas, imprime e atualiza variáveis
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat/100,auxLon/100,count);
			auxLat = (float)data[i]/1000;
			auxLon = (float)((data[i]%1000)-930);
			count = 1;
		}
		if(i==nLinhas-1)													//último valor, imprime
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat/100,auxLon/100,count);
		}
	}
	free(data);
	return 0;
}

int truncate(float val)
{
	val *= 100;
    if (val>0)
    {
    	return (int)floor(val);
    }
    else
    {
    	return (int)ceil(val);
    }
}

void radixSort(int* data, int nLinhas)
{
	int i, maior=data[0], casa = 1;
	int* aux = (int*)malloc(nLinhas*sizeof(int));
	for(i=1;i<nLinhas;i++)
	{
		if(data[i] > maior)
		{
			maior = data[i];
		}
	}
	while(maior/casa > 0)
	{
		int* balde = (int*)calloc(10,sizeof(int));
		for(i=0;i<nLinhas;i++)
		{
			balde[(data[i] / casa) % 10]++;
		}
		for(i=1;i<10;i++)
		{
			balde[i] += balde[i-1];
		}
		for(i=nLinhas-1;i>=0;i--)
		{
			balde[(data[i]/casa)%10]--;
			aux[balde[(data[i]/casa)%10]] = data[i];
		}
		for(i=0;i<nLinhas;i++)
		{
			data[i] = aux[i];
		}
		casa*=10;
		free(balde);
	}
	free(aux);
}
