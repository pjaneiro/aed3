#include <cstdlib>
#include <cstdio>
#include <cfloat>
#include <cmath>

typedef struct
{
	float latitude, longitude;
} registry;

float truncate(float);
void quickSort(registry*, int, int);
int divide(registry*,int,int);

int main(int argc, char *argv[])
{
	int nLinhas, count, i;
	float auxLat, auxLon;
	fscanf(stdin,"%d",&nLinhas);
	registry* data = (registry*)malloc(nLinhas*sizeof(registry));
	for(i=0;i<nLinhas;i++)
	{
		fscanf(stdin,"%*d,%*d,%*d-%*d-%*d %*d:%*d:%*d,%f,%f",&((data+i)->latitude),&((data+i)->longitude));
		(data+i)->latitude = truncate((data+i)->latitude);
		(data+i)->longitude = truncate((data+i)->longitude);
	}
	quickSort(data,0,nLinhas-1);
	for(i=0;i<nLinhas;i++)
	{
		if(i==0)
		{
			auxLat = (data+i)->latitude;
			auxLon = (data+i)->longitude;
			count = 1;
		}
		else if((data+i)->latitude==auxLat && (data+i)->longitude==auxLon)
		{
			count++;
		}
		else
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat,auxLon,count);
			auxLat = (data+i)->latitude;
			auxLon = (data+i)->longitude;
			count = 1;
		}
		if(i==nLinhas-1)
		{
			fprintf(stdout, "%.2f,%.2f,%d\n",auxLat,auxLon,count);
		}
	}
	free(data);
	return 0;
}

float truncate(float val)
{ 
	val *= 100;
    if (val>0)
    	return (float)floor(val)/100;
    else
    	return (float)ceil(val)/100;
}

void quickSort(registry* v, int li, int ls)
{
	int j;
	if(li<ls)
	{
		j=divide(v,li,ls);
		quickSort(v,li,j-1);
		quickSort(v,j+1,ls);
	}
}

int divide(registry* x,int li,int ls)
{
	registry a, temp;
	int down, up;
	a = *(x+li);
	down = li;
	up = ls;
	while(down<up)
	{
		while(((x+down)->latitude<a.latitude || ((x+down)->latitude==a.latitude && (x+down)->longitude<=a.longitude)) && down<ls)
		{
			down++;
		}
		while((x+up)->latitude>a.latitude || ((x+up)->latitude==a.latitude && (x+up)->longitude>a.longitude))
		{
			up--;
		}
		if(down<up)
		{
			temp = *(x+down);
			*(x+down) = *(x+up);
			*(x+up) = temp;
		}
	}
	*(x+li) = *(x+up);
	*(x+up) = a;
	return up;
}
